from django.http import HttpResponse, JsonResponse
from django.shortcuts import render


def index(request):
    return HttpResponse("Hello, world. You're at the student index.")


def index_template(request):
    return render(request, 'index.html')


def index_json(request):
    return JsonResponse({'message': 'Hello, world!'})


def index_with_params(request, name):
    return HttpResponse(f"Hello, {name}!")


def index_with_get_params(request):
    name = request.GET.get('name')
    surname = request.GET.get('surname')
    if not name and not surname:
        name = 'Students'
        surname = 'of the University of Pretoria'
    return HttpResponse(f"Hello, {name} {surname}!")


def index_with_params_to_template(request):
    name = request.GET.get('name')
    surname = request.GET.get('surname')
    return render(request, 'index_with_params.html', context={
        'name': name,
        'surname': surname,
    })


def student_form(request):
    return render(request, 'student_form.html')


def put_student_params(request):

    name = request.POST.get('name')
    surname = request.POST.get('surname')

    return HttpResponse(f"Student info: {name} {surname}")

