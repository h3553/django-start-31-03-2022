"""test_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from student.views import index, index_template, index_json, index_with_params, index_with_get_params, \
    index_with_params_to_template, put_student_params, student_form

urlpatterns = [
    path('admin/', admin.site.urls),
    path('students/', index),
    path('students_template/', index_template),
    path('students_json/', index_json),
    path('student/<str:name>', index_with_params),
    path('student_detail/', index_with_get_params),
    path('student_params_template/', index_with_params_to_template),

    path('student_post/', put_student_params),
    path('student_post_template/', student_form),
]
